require("dplyr")
source("functions/helpers.R")

# generate the results of the binomial glm for different sigma
dir <- "data/genCML/"
sigma_dirs <- dir(dir, full.names = T)[dir(dir) %>% grep(pattern = "sigma*")]

performanceBinomialGLM_sigma <- do.call("rbind", lapply(sigma_dirs, function(d) {
  return(performanceBinomialGLM(paste(d, "test", sep = "/")))
}))
