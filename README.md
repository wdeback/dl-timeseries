
- Create python environment, here named `dl`

```
conda create -n dl python=3.6 keras ipykernel matplotlib scikit-learn
```

- Activate environment

```
source activate dl
```

- Run notebook

```
jupyter notebook
```